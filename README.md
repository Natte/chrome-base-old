[![license](https://img.shields.io/github/license/mashape/apistatus.svg) ](LICENSE)

# Chrome Client
A simple to use modded Minecraft client that helps you defeat those pesky kiddies. 

## Contributing
Feel free to contribute to this project. Just make sure you pull the latest version before making a pull request.

## Issues
If you any issues please report them [here](https://github.com/Haaaqs/chrome-client/issues).
